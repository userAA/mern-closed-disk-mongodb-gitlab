gitlab page https://gitlab.com/userAA/mern-closed-disk-mongodb-gitlab.git
gitlab comment mern-closed-disk-mongodb-gitlab

проект mern-closed-disk (хранилище файлов)
используемые технологии на фронтенде:
    axios,
    react,
    react-dom,
    react-redux,
    react-router-dom,
    redux,
    redux-devtools-extension,
    react-transition-group,
    redux-thunk;
используемые технологии на бекэнде: 
server application using methods
    bcryptjs,
    config,
    express,
    express-fileupload,
    express-validator,
    jsonwebtoken,
    mongoose,
    uuid;

Имеется регистрация пользователя.
Имеется авторизация пользователя.
Аватарку пользователя можно менять.
У каждого пользователя можно в неограниченном количестве 
добавлять папки, удалять папки, добавлять файлы,
удалять файлы в неограниченном количестве переходить в папку и выходить из нее.
Файлы каждой папки можно сортировать по дате создания, имени и типу.
Файл в каждой текущей папке можно находить по имени.
Можно перетаскивать файлы при их любом распределении.